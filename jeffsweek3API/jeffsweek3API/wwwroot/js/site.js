﻿const baseurl = 'https://jsonplaceholder.typicode.com/users/';

let listAllNames = (theUrl) => {

    let xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = ((theUrl) => {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {

            let idslist = document.getElementById('ids');
            let namesList = document.getElementById('names');
            let result = JSON.parse(xmlHttp.responseText);

            if (result.length > 0) {

                for (let i = 0; i < result.length; i++) {

                    let listitems = document.createElement('li');
                    listitems.setAttribute('class', 'list-group-item');
                    listitems.innerHTML = result[i].id + " - " + result[i].name + " - " + result[i].username + " - " + result[i].email + " - " + result[i].address.street + " - " + result[i].phone;
                    namesList.appendChild(listitems);
                }
            }
        }
    }).bind(undefined, theUrl);

    xmlHttp.open("GET", theUrl, true); // false for synchronous request
    xmlHttp.send(null);
}

let getListNames = () => {
    listAllNames(baseurl)
}